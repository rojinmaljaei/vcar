#overview

Vcar is a part of a bigger car rental web application system. In this system, car owners can add/edit/delete their vehicles.

In the side bar of the page have filters which allows a user to customize and narrow down the search result of a particular vehicle. 

on click on more info, userc can retrieve a product info. In this view, we will provide a comprehensive data about a car.

To create a new vehicle, please click on add vehicle. It will open up a vehicle registration form in a modal.



#Designer Note:

Before starting the design, I looked at the data and tried to understand the platform and prioritized them. Therefore some of the data of a vehicle might be more important to an audience than others. I tried to show them in the vehicle list view and avoid show extra data to not to make a busy design and also make an uncomfortable user experience. 

To create a better UI, I thought this application must be a single-page application and avoid refreshing the page. In another hand, for faster browsing the vehicles, I thought modals are the best options to retrieve more info about a product or add a new product.

Thanks to Bootstrap 4, I could make a responsive UI which can be used in different devices with different screen sizes. 



#Questions:


- How long did it take for you to finish the assignment?
It took around 20 hours to finish it. Of course not all of this time was related to coding. There is around 3 to 4 hours of time on planing, research and brainstorming. 

- What problems and/or issues did you encounter performing the assignment?
Lack of a sketch or a blueprint from a product manager.

- Do you have any suggestions that may help us improve this assignment?
Designers are not like developers who have fixes tasks with clear outcome and income. If you really want to assess a designer, you should give more time to the candidate to not to only complete the task. The designer should have enough time to bring perfection to the final product. Then you can assess the quality of design better. 